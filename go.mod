module gitlab.com/imaginadio/golang/examples/machine-learning/anomaly-detection

go 1.20

require github.com/lytics/anomalyzer v0.0.0-20151102000650-13cee1061701

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/drewlanenga/govector v0.0.0-20220726163947-b958ac08bc93 // indirect
	github.com/kr/pretty v0.3.1 // indirect
)
