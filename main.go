package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/lytics/anomalyzer"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s MAX\n", os.Args[0])
		return
	}

	max, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	conf := &anomalyzer.AnomalyzerConf{
		Sensitivity: 0.1,
		UpperBound:  5,
		LowerBound:  anomalyzer.NA,
		ActiveSize:  1,
		NSeasons:    4,
		Methods:     []string{"diff", "fence", "magnitude", "ks"},
	}

	data := []float64{}
	randSource := rand.NewSource(time.Now().Unix())
	rnd := rand.New(randSource)

	for i := 0; i < max; i++ {
		data = append(data, float64(rnd.Intn(max)))
	}
	fmt.Println(data)

	anom, err := anomalyzer.NewAnomalyzer(conf, data)
	if err != nil {
		panic(err)
	}

	prob := anom.Push(8)
	fmt.Println("Anomalous probability:", prob)
}
